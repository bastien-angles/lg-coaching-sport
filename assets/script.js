const visibilityLimit = window.innerHeight;
const elevatorElement = document.getElementById("elevator");

function openMenu() {
  document.getElementById("sidebar-full").style.left = "0";
  document.body.classList.add("no-mobile-scroll");
}

function closeMenu() {
  const sidebarFullWidth = document.getElementById("sidebar-full").offsetWidth;
  document.getElementById("sidebar-full").style.left = `-${sidebarFullWidth}px`;
  document.body.classList.remove("no-mobile-scroll");
}

function elevatorVisibility() {
  if (window.scrollY > visibilityLimit) {
    elevatorElement.classList.add("visible");
  } else {
    elevatorElement.classList.remove("visible");
  }
}

// Init elevator visibility
elevatorVisibility();

// Elavator hide on slider
document.addEventListener("scroll", () => {
  elevatorVisibility();

  // Close menu if not on homepage
  if (window.scrollY > visibilityLimit) {
    closeMenu();
  }
});

elevatorElement.addEventListener('click', e => {
  window.scrollTo({top: 0, behavior: "smooth"});
});

document.addEventListener('click', event => {
  // Desktop on click outside menu, close it
  const isClickInside = document.getElementById("sidebar-full").contains(event.target);
  const eventId = event.target.id;

  // On menu click, force to close menu
  const isClickOnMenu = event.target.classList.contains("menu-link");

  if ((!isClickInside && eventId != "open-menu" && eventId != "open-mail") || isClickOnMenu) {
    closeMenu();
  }
});

// Open menu on click
document.getElementById("open-menu").addEventListener('click', e => {
  openMenu();
});

document.getElementById("open-mail").addEventListener('click', e => {
  openMenu();
});

// Close menu on click
document.getElementById("close-menu").addEventListener('click', e => {
  closeMenu();
});